import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'String-Check': '000c7d69c01e7268adc1ab0ba1f6f1b7777b4cfd13da8e29e4e1bfc2cb0eb2b63fb1127d99810f0d',
});

@Injectable({
    providedIn: 'root'
})


export class ApiService {


    baseUrl = "https://paramantra.us/paramantra/api/mdata_0520";


    constructor(private http: HttpClient,
        private Storage: Storage) { }

    // Handle API errors
    handleError(error: HttpErrorResponse) {

        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {

            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };


    getMethod(service, data, accessToken) {

        var accessToken = accessToken;


        const headersi = new HttpHeaders({
            'Content-Type': 'application/json',
            'String-Check': '000c7d69c01e7268adc1ab0ba1f6f1b7777b4cfd13da8e29e4e1bfc2cb0eb2b63fb1127d99810f0d',
            'Authorization': 'Bearer ' + accessToken
        });

        return new Promise((resolve, reject) => {
            this.http.get(this.baseUrl + service + data, { headers: headersi }).subscribe(response => {
                resolve(response);
            },
                (error) => {
                    reject(error);
                });
        });
    }


    postMethod(service, data) {

        return new Promise((resolve, reject) => {
            this.http.post(this.baseUrl + service, data, { headers: headers }).subscribe(response => {
                resolve(response);
            },
                (error) => {
                    reject(error);
                });
        });
    }

    login(body: any) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Basic cGFyYW1hbnRyYTpwbXRyXzkwMQ==',
            'Content-Length': '<calculated when request is sent>',
            'Host': '<calculated when request is sent>',
            'Accept-Encoding': 'gzip, deflate, br',
            'X-API-KEY': body.user_name
        });
        return this.http.post(this.baseUrl + '/login', body, { headers: headers })
    }




}
