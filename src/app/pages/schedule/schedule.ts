import { Component, ViewChild, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, IonList, IonRouterOutlet, LoadingController, ModalController, ToastController, Config } from '@ionic/angular';

import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { Subscription } from 'rxjs';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  styleUrls: ['./schedule.scss'],
})
export class SchedulePage implements OnInit {
 

  userName : any;
  todayDate : any;

  userDetailsSubscription : Subscription;
  userDetails: any = {};

  constructor(
    public alertCtrl: AlertController,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    public toastCtrl: ToastController,
    public userData: UserData,
    public config: Config
  ) { 

   this.userDetails = this.userData.getUserDetails();
    console.log("userDetails ", this.userDetails); 
    if (Object.keys(this.userDetails).length != 0) {
      this.userName = this.userDetails.user_name ;
      console.log("userName : ", this.userName);
    } else {
      console.log("subscription value is empty ");
    }
    
    
    this.userDetailsSubscription = this.userData.userDetailsChanged$.subscribe((res) => {
      console.log("%^%^%^%^%^%^%^&^&^&^&^  User Details subscribed from the the observable in schedule page  %^%^%^%^%^%^%^&^&^&^&^");
      console.log("loginvalue : ", res);
      if (Object.keys(res).length != 0) {
        this.userDetails = res;
        this.userName = this.userDetails.user_name ;
        console.log("userName : ", this.userName);
      } else {
        console.log("subscription value is empty ");
      }
    })
  }

  ngAfterViewInit() {
    this.getDate();
    this.getUsername();
  }

  ngOnInit() {
  
  }
  


  getDate(){
    var date = new Date();

    this.todayDate = date 
    console.log("Today Date : ", this.todayDate);
    
  }
  

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.userName = username;
    });
  }


  goToPage(page){
    let navigationExtras : NavigationExtras = {
      state : {
        detailType : page
      }
    }
 
    this.router.navigate(['session'], navigationExtras);
  }

}
