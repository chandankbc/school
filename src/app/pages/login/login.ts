import { Component } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';
import { LoadingController, MenuController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from '../../services/api/api.service';
import { catchError, retry, timeout } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  login: UserOptions = { username: '', password: '' };
  submitted = false;

  loginForm:FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  token: any;
  platformType: string;
  userRes: any;
  message: any;

  constructor(
    public userData: UserData,
    public router: Router,
    private toastController : ToastController,
    private loadingController : LoadingController,
    private menu: MenuController,
    private apiService : ApiService,
    private platform : Platform,
    private storage : Storage
  ) { }

  ionViewWillEnter() {
    this.menu.enable(false);

    this.platform.ready().then(() => {

      if (this.platform.is('android')) {
        this.platformType = 'android';
        // this.showToast('PlatForm Type :',' ANDROID ', 5000);
        console.log("Platform Type : ANDROID");
      } else if (this.platform.is('ios')) {
        this.platformType = 'ios';
        // this.showToast('PlatForm Type :',' IOS ', 5000);
        console.log("Platform Type : IOS");
      } else {
        this.platformType = 'undefined';
        // this.showToast('PlatForm Type :',' UNDEFINED ', 5000);
        console.log("Platform Type : UNDEFINED");
      }
    });
  }


  ngOnInit() {


   }

   async onLogin(form: NgForm) {
    let body = {};
      const loader = await this.loadingController.create({
        showBackdrop: false,
        spinner: "dots",
        translucent: true,
        cssClass: "loaderClass"
      });
      loader.present();

      this.submitted = true;
      console.log("Login Form :", form);
      body = {
        'user_name': form.value.username,
        'pass_word': form.value.password
      }

      console.log("login body :", body);
      
      if (form.valid) {
        console.log("Valid login form");

        let PostData = {
          email:form.value.username,
          password:form.value.password,
          usertype:"customer"
        }

        this.apiService.postMethod("api/login?", PostData).then((response : any) => {
      
          console.log(response);
    
          if(response["userdata"].status==0)
          {    loader.dismiss();
            // this.apiService.nativeToast("These credentials do not match our records");
            this.toastWithoutHeader("These credentials do not match our records", 3000);
          }
          else
          {

            loader.dismiss();
            this.router.navigate(['app/tabs/schedule']);

            let userName = response.userdata.first_name + ' ' + response.userdata.last_name;
            console.log("userName : ", userName);
            
            this.userData.login(userName);
            this.userData.updateUserDetails(response.userdata);
            this.toastWithoutHeader('Login Successful', 3000);
            
          }
    
          },
          (error) => {
            loader.dismiss();
          console.log(error);
          this.toastWithoutHeader(error.error.message, 3000)
          });




      } else {
        loader.dismiss();
        this.toastWithoutHeader("Username and Password are mandatory", 3000);
      }
    }


    toastWithoutHeader(message, duration: number) {
      const toast = this.toastController.create({
        message: message,
        duration: duration
      }).then((toastData) => {
        console.log(toastData);
        toastData.present();
      });
    }
  
  
    hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }

    ionViewWillLeave(){
      this.login.username= "";
      this.login.password = "";
      console.log("Login body : ", this.login);
      this.menu.enable(true);
    }


    async onLoginn(form: NgForm) {
        let body = {};
        const loader = await this.loadingController.create({
          showBackdrop: false,
          spinner: "dots",
          translucent: true,
          cssClass: "loaderClass"
        });
        loader.present();
  
        this.submitted = true;
        console.log("Login Form :", form);
        body = {
          'user_name': form.value.username,
          'pass_word': form.value.password,
          'unique_device_id': this.token,
          'device_platform': this.platformType,
        }
  
        console.log("login body :", body);
        this.storage.set('fingerAuth', body).then(() => {
          console.log("FingerAuth data saved success fully.. ")
        })
        if (form.valid) {
          this.apiService.login(body)
            .pipe(
              timeout(45000),
              retry(1),
              catchError(this.handleError)
            )
            .subscribe((res: any) => {
              console.log("loogin Response :", res);
              this.userRes = res;
  
              if (res[0].status == 200) {
                var response = res[0];
                var loginInfo = response.response[0];
                console.log("Login info", loginInfo);
            

               
    
                let userName = loginInfo.user_name;
                console.log("userName : ", userName);
                
                this.userData.login(userName);
                this.userData.updateUserDetails(loginInfo);
                this.toastWithoutHeader('Login Successful', 3000);

                setTimeout(() => {
                   
                  loader.dismiss();
                  this.router.navigate(['app/tabs/schedule']);
                }, 1000)
                
  
        
  
              }
  
              else {
                loader.dismiss();
              }
            }, error => {
              setTimeout(() => {
                console.log("error test", error);
                if (error.name && error.name == 'TimeoutError') {
                  this.message = error.message;
                  this.toastWithoutHeader( this.message + ', Please check your network and try again later.', 3000);
                  loader.dismiss();
                } else if (error.status == 401) {
                  // this.message = error.message;
                  this.toastWithoutHeader('Invalid Credential', 3000);
                  loader.dismiss();
                } else {
                  this.toastWithoutHeader('Something went wrong!', 3000);
                  loader.dismiss();
                }
              }, 500)
  
            })
        } else {
          loader.dismiss();
          this.toastWithoutHeader("Username and Password are mandatory", 3000);
        }
  
    } 

    handleError(error: HttpErrorResponse) {
      return throwError(error);
    };
  
  
}
