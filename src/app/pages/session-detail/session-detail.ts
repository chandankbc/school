import { Component } from '@angular/core';

import { ConferenceData } from '../../providers/conference-data';
import { ActivatedRoute, Router } from '@angular/router';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'page-session-detail',
  styleUrls: ['./session-detail.scss'],
  templateUrl: 'session-detail.html'
})
export class SessionDetailPage {

  defaultHref = '';
  detailType: any ;

  constructor(
    private dataProvider: ConferenceData,
    private userProvider: UserData,
    private route: ActivatedRoute,
    private router : Router
  ) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.detailType = {};
        this.detailType = this.router.getCurrentNavigation().extras.state.detailType;
        console.log("recieved data from schedule", this.detailType);
        if (this.detailType) {
        //  this.
        }
      }
    })
  }

  ionViewWillEnter() {
  
  }

  ionViewDidEnter() {
    this.defaultHref = `/app/tabs/schedule`;
  }

}
